# Django GraphQL API

Hello there. This is an ongoing Django GraphQL api boilerplate aimed to help startup a new project.

## Some Features Already Here

I'm already add some help the development setup and start project

1. [Django](https://www.djangoproject.com/)
1. [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/)
1. [Postgre SQL](https://www.postgresql.org/)
1. [Nginx](https://www.nginx.com/)
1. [Pre-Commit](https://pre-commit.com/)
1. [Black](https://github.com/ambv/black)
1. [Flake8](http://flake8.pycqa.org/en/latest/)

## Useful Tools

Some Python useful tools are or will be installed with time

1. [IPython](https://ipython.org/)

## Run Project Locally

You will need to clone this project and setup new virtual environment or use an existing one

1. Within your virtual environment install and init

   ```bash
   pip install pre-commit
   pre-commit install
   ```

1. Clone this project and step in

   ```bash
   git remote add origin git@gitlab.com:adroaldof/django-graphql-api.git
   cd django-graphql-api
   ```

1. Start project and database at once with the follow command ⏯

   ```bash
   docker-compose up
   ```

   Now you can start to make your desired changes

1. When you decide to stop your work for today, ensure all containers are stopped ⏹

   ```bash
   docker-compose down
   ```

## Some References

Here is the space to include some references

1. [How to Dockerize a Django Application](https://medium.com/backticks-tildes/how-to-dockerize-a-django-application-a42df0cb0a99)
1. [Dockerizing Django with Postgres, Gunicorn, and Nginx](https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/)
1. [Building Django HTTP APIs with GraphQL and Graphene](https://www.techiediaries.com/django-graphql-tutorial/)
1. [Building a GraphQL API with Django](https://stackabuse.com/building-a-graphql-api-with-django/)
1. [Mutation and Query In GraphQL Using Python/Django PART 1](https://medium.com/@jamesvaresamuel/mutation-and-query-in-graphql-using-python-django-part-1-5bd4bce2b2a3)
1. [Consistent Python code with Black](https://www.mattlayman.com/blog/2018/python-code-black/)
1. [Automate Python workflow using pre-commits: black and flake8](https://ljvmiranda921.github.io/notebook/2018/06/21/precommits-using-black-and-flake8/)
1. [Using a custom user model when starting a project](https://docs.djangoproject.com/en/2.1/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project)
1. [Django Log In with Email not Username](https://wsvincent.com/django-login-with-email-not-username/)
1. [How to use email as username for Django authentication (removing the username)](https://www.fomfus.com/articles/how-to-use-email-as-username-for-django-authentication-removing-the-username)

---

Enjoy it 😉
