from graphql_relay.node.node import from_global_id


def get_first_object_where(instance, filter_key, value, otherwise=None):
    try:
        instance = instance.objects.filter(**{filter_key: value})

        if len(instance) == 0:
            raise Exception("Not found")

        return instance[0]
    except Exception as error:
        return otherwise or error


def get_object(instance, relayId, otherwise=None):
    try:
        primary_key = from_global_id(relayId)[1]
        instance = instance.objects.get(pk=primary_key)
        return instance
    except Exception as error:
        return otherwise or error


def create_or_update_instance(instance, args, exceptions=["id"]):
    if instance:
        [
            setattr(instance, key, value)
            for key, value in args.items()
            if key not in exceptions
        ]
    instance.save()
    return instance


def delete_instance(instance):
    instance_copy = instance
    instance.delete()
    return instance_copy


def get_errors(error):
    fields = error.message_dict.keys()
    messages = ["; ".join(message) for message in error.message_dict.values()]
    errors = [info for pair in zip(fields, messages) for info in pair]
    return errors
