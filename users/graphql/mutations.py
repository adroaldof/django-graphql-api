import graphql_jwt
import graphene

from django.core.exceptions import FieldError

from users.models import User
from users.graphql.types import UserType

from helpers.database import get_errors


class CreateUser(graphene.ClientIDMutation):
    created = graphene.Field(UserType)
    errors = graphene.List(graphene.String)

    class Input:
        email = graphene.NonNull(graphene.String)
        password = graphene.NonNull(graphene.String)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        try:
            user = User(email=input.get("email"))
            user.set_password(input.get("password"))
            user.save()

            return cls(created=user)
        except FieldError as error:
            return cls(created=None, errors=get_errors(error))


class Mutations(graphene.ObjectType):
    create_user = CreateUser.Field()
    token_auth = graphql_jwt.relay.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.relay.Verify.Field()
    refresh_token = graphql_jwt.relay.Refresh.Field()
    revoke_token = graphql_jwt.relay.Revoke.Field()
