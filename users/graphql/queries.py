from django.contrib.auth import get_user_model
from graphene import ObjectType, Field
from graphene_django import DjangoConnectionField
from graphql_jwt.decorators import login_required, staff_member_required

from users.graphql.types import UserType


class Queries(ObjectType):
    me = Field(UserType)
    users = DjangoConnectionField(UserType)

    @login_required
    def resolve_me(self, info):
        user = info.context.user
        return user

    @staff_member_required
    def resolve_users(self, info):
        return get_user_model().objects.all()
