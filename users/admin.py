from django.contrib import admin

from users.models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    model = User
    list_display = ["name", "email", "date_joined", "is_superuser", "is_staff"]
    list_filter = ("is_superuser", "is_staff", "is_active")
    ordering = ("name", "email")
