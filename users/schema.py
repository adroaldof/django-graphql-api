from graphene import Schema

from users.graphql.mutations import Mutations
from users.graphql.queries import Queries

schema = Schema(query=Queries, mutation=Mutations)
