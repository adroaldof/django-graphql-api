from graphene import Schema

from products.graphql.queries import Queries
from products.graphql.mutations import Mutations

schema = Schema(query=Queries, mutation=Mutations)
