from django.db.models import Q
from graphene import List, ObjectType, Node, String, relay
from graphene_django.filter import DjangoFilterConnectionField

from products.graphql.types import ProductNode, ProductFilter


class Queries(ObjectType):
    product = relay.Node.Field(ProductNode)
    products = DjangoFilterConnectionField(ProductNode, filterset_class=ProductFilter)
