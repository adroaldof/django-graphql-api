from django.core.exceptions import ValidationError
from graphene import Mutation, NonNull, String, Float, Field, List, ObjectType, relay
from graphql import GraphQLError
from graphql_jwt.decorators import login_required

from products.models import Product
from products.graphql.types import ProductNode

from helpers.database import (
    create_or_update_instance,
    delete_instance,
    get_errors,
    get_object,
)


class CreateProduct(relay.ClientIDMutation):
    created = Field(ProductNode)
    errors = List(String)

    class Input:
        name = NonNull(String)
        description = String()
        price = Float()

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        try:
            product = Product()
            created = create_or_update_instance(product, input)
            return cls(created=created)
        except ValidationError as error:
            return cls(created=None, errors=get_errors(error))


class UpdateProduct(relay.ClientIDMutation):
    updated = Field(ProductNode)
    errors = List(String)

    class Input:
        id = NonNull(String)
        name = String()
        description = String()
        price = Float()

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        try:
            instance = get_object(Product, input["id"])

            if instance:
                updated = create_or_update_instance(instance, input)
                return cls(updated=updated)
        except ValidationError as error:
            return cls(updated=None, errors=get_errors(error))


class DeleteProduct(relay.ClientIDMutation):
    deleted = Field(ProductNode)

    class Input:
        id = String(required=True)

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        try:
            instance = get_object(Product, input["id"])

            if instance:
                deleted = delete_instance(instance)
                return cls(deleted=deleted)
        except ValidationError as error:
            return cls(updated=None, errors=get_errors(error))


class Mutations(ObjectType):
    create_product = CreateProduct.Field()
    update_product = UpdateProduct.Field()
    delete_product = DeleteProduct.Field()
