from graphene import relay
from graphene_django import DjangoObjectType
from django_filters import FilterSet

from products.models import Product


class ProductFilter(FilterSet):
    class Meta:
        model = Product
        fields = ["name", "description"]


class ProductNode(DjangoObjectType):
    class Meta:
        model = Product
        interfaces = (relay.Node,)
