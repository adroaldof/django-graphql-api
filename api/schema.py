from graphene import ObjectType, Schema

from products.schema import Queries as ProductQueries, Mutations as ProductMutations
from users.schema import Queries as UsersQueries, Mutations as UsersMutations


class Query(ProductQueries, UsersQueries, ObjectType):
    pass


class Mutation(ProductMutations, UsersMutations, ObjectType):
    pass


schema = Schema(query=Query, mutation=Mutation)
