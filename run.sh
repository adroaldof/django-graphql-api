#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres at $SQL_HOST:$SQL_PORT"

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

# python manage.py flush --no-input  # Destroy all database data
python manage.py migrate  # Run migrations
python manage.py collectstatic --no-input

exec "$@"
