FROM python:3.7

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  net-tools \
  netcat \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

RUN pip install --upgrade pip
RUN pip install pipenv
COPY ./Pipfile /usr/src/app/Pipfile
RUN pipenv install --skip-lock --system --dev

COPY . /usr/src/app/

COPY ./run.sh /usr/src/app/run.sh
ENTRYPOINT ["/usr/src/app/run.sh"]
